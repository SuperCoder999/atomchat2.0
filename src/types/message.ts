interface BaseMessage {
    id: string;
    text: string;
    user: string;
    avatar: string | null;
    userId: string;
}

export interface Message extends BaseMessage {
    editedAt: Date | null;
    createdAt: Date;
    likeCount: number;
    likers: string[];
    dislikeCount: number;
    dislikers: string[];
}

export interface PartialMessage {
    text?: string;
    user?: string;
    avatar?: string | null;
    userId?: string;
    editedAt?: Date | null;
    createdAt?: Date;
    likeCount?: number;
    likers?: string[];
    dislikeCount?: number;
    dislikers?: string[];
}

export interface NotCompiledMessage extends BaseMessage {
    editedAt: string;
    createdAt: string;
}

export function compileMessage(message: NotCompiledMessage): Message {
    return {
        ...message,
        createdAt: new Date(message.createdAt),
        editedAt: message.editedAt ? new Date(message.editedAt) : null,
        likeCount: 0,
        likers: [],
        dislikeCount: 0,
        dislikers: []
    };
}
