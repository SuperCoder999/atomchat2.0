import { Message, PartialMessage } from "./message";

interface BaseAction {
    type: string;
}

export interface MessageListState {
    messages: Message[];
    editingID?: string;
}

export interface UsersListState {
    users: string[]
}

export interface State {
    messages: MessageListState,
    users: UsersListState
}

export interface SetMessagesAction extends BaseAction {
    messages: Message[];
}

export interface AddMessageAction extends BaseAction {
    message: Message;
}

interface IDAction extends BaseAction {
    id: string;
}

export interface DeleteMessageAction extends IDAction {}

export interface SetEditingMessageAction extends BaseAction {
    id?: string;
}

export interface UpdateMessageAction extends IDAction {
    message: PartialMessage;
}

export interface SetUsersAction extends BaseAction {
    users: string[];
}
