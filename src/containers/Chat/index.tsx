import React from "react";
import Header from "../../components/Header";
import { Message, PartialMessage } from "../../types/message";
import { getMessages } from "../../services/message";
import Spinner from "../../components/Spinner";
import MessageList from "../MessageList";
import MessageInput from "../../components/MessageInput";
import EditMessageForm from "../../components/EditMessageForm";
import { Grid, Image } from "semantic-ui-react";
import logo from "../../images/logo.svg";
import { genUUID } from "../../helpers/uuid";
import Copyright from "../../components/Copyright";
import UsersList from "../UsersList";

import {
    MESSAGE_ME_USER,
    IS_MOBILE,
    MESSAGE_ME_USER_ID
} from "../../config";

import {
    setMessages,
    updateMessage,
    addMessage,
    deleteMessage,
    setEditingMessage
} from "../MessageList/actions";

import {
    SetMessagesAction,
    UpdateMessageAction,
    State,
    AddMessageAction,
    DeleteMessageAction,
    SetEditingMessageAction
} from "../../types/redux";

import { connect } from "react-redux";
import { Dispatch } from "redux";

interface ChatProps {
    messages: Message[];
    editingMessageID?: string;
    setMessages: (messages: Message[]) => void;
    updateMessage: (id: string, message: PartialMessage) => void;
    addMessage: (message: Message) => void;
    deleteMessage: (id: string) => void;
    setEditingMessage: (id?: string) => void;
}

interface ChatState {
    requestedMessages: boolean;
}

class Chat extends React.Component<ChatProps, ChatState> {
    public constructor(props: ChatProps) {
        super(props);

        this.state = {
            requestedMessages: false
        }
    }

    public componentDidMount() {
        getMessages()
            .then((messages: Message[]): void => {
                this.setState({
                    requestedMessages: true
                });

                this.props.setMessages(messages);
            });
    }

    private addMessage(text: string): void {
        this.props.addMessage(({
            id: genUUID(),
            user: MESSAGE_ME_USER,
            avatar: null,
            text,
            createdAt: new Date(),
            editedAt: null,
            likers: [],
            likeCount: 0,
            dislikers: [],
            dislikeCount: 0,
            userId: MESSAGE_ME_USER_ID
        } as Message));
    }

    private showEditMessageForm(id: string): void {
        this.props.setEditingMessage(id);
    }

    private hideEditMessageForm(): void {
        this.props.setEditingMessage();
    }

    private editMessage(text: string): void {
        if (this.props.editingMessageID) {
            this.props.updateMessage(this.props.editingMessageID, { text });
            this.props.setEditingMessage();
        }
    }

    private deleteMessage(id: string): void {
        this.props.deleteMessage(id);
    }

    private reactMessage(id: string, isLike: boolean): void {
        const messages = [...this.props.messages];
        const index: number = messages.findIndex((message: Message): boolean => id === message.id);
        const message: Message = { ...messages[index] };
        let diff: number;

        if (isLike) {
            diff = message.likers.includes(MESSAGE_ME_USER_ID) ? -1 : 1;
            message.likeCount += diff;
        } else {
            diff = message.dislikers.includes(MESSAGE_ME_USER_ID) ? -1 : 1;
            message.dislikeCount += diff;
        }

        if (diff > 0) {
            if (isLike) {
                message.likers.push(MESSAGE_ME_USER_ID);
            } else {
                message.dislikers.push(MESSAGE_ME_USER_ID);
            }
        } else {
            if (isLike) {
                message.likers.splice(message.likers.indexOf(MESSAGE_ME_USER_ID), 1);
            } else {
                message.dislikers.splice(message.dislikers.indexOf(MESSAGE_ME_USER_ID), 1);
            }
        }

        this.props.updateMessage(id, { ...message });
    }

    private likeMessage(id: string): void {
        this.reactMessage(id, true);
    }

    private dislikeMessage(id: string): void {
        this.reactMessage(id, false);
    }

    public render(): JSX.Element {
        const { requestedMessages } = this.state;
        const { editingMessageID, messages } = this.props;
        const logoSize: "small" | "big" | "mini" | "tiny" | "medium" | "large" | "huge" | "massive" = IS_MOBILE ? "medium" : "big";

        if (!requestedMessages) {
            return <Spinner />
        }

        const header: JSX.Element = <Header />;
        const usersList: JSX.Element = <UsersList />
        const logoElement: JSX.Element = <Image src={logo} size={logoSize} centered className="rotating" alt="Logo" />;

        const forms: JSX.Element = <>
            <MessageInput createMessage={(text: string): void => this.addMessage(text)} />
            {editingMessageID
                ? <EditMessageForm
                    editMessage={(text: string): void => this.editMessage(text)}
                    cancelEditMessage={() => this.hideEditMessageForm()}
                    defaultMessage={
                        (messages
                            .find((sMessage: Message): boolean => sMessage.id === this.props.editingMessageID) as Message)
                            .text
                    }
                />
                : ""}
        </>;

        const messagesElement: JSX.Element = <MessageList
            showingMessagesLimitStep={10}
            editMessage={(id: string): void => this.showEditMessageForm(id)}
            deleteMessage={(id: string): void => this.deleteMessage(id)}
            likeMessage={(id: string): void => this.likeMessage(id)}
            dislikeMessage={(id: string): void => this.dislikeMessage(id)}
        />;

        const copyright: JSX.Element = <Copyright />;

        if (IS_MOBILE) {
            return (
                <>
                    {header}
                    {logoElement}
                    {forms}
                    {usersList}
                    {messagesElement}
                    {copyright}
                </>
            );
        }

        return (
            <>
                {header}
                <Grid columns="2">
                    <Grid.Column>
                        {logoElement}
                        {forms}
                        {usersList}
                    </Grid.Column>
                    <Grid.Column>
                        {messagesElement}
                    </Grid.Column>
                </Grid>
                {copyright}
            </>
        );
    }
}

function mapStateToProps(state: State): { messages: Message[], editingMessageID?: string } {
    return {
        editingMessageID: state.messages.editingID,
        messages: state.messages.messages
    };
}

type A = SetMessagesAction | UpdateMessageAction | AddMessageAction | DeleteMessageAction | SetEditingMessageAction;

function mapDispatchToProps(dispatch: Dispatch<A>) {
    return {
        setMessages: (messages: Message[]): SetMessagesAction => dispatch(setMessages(messages)),
        updateMessage: (id: string, newMessage: PartialMessage): UpdateMessageAction => dispatch(updateMessage(id, newMessage)),
        addMessage: (message: Message): AddMessageAction => dispatch(addMessage(message)),
        deleteMessage: (id: string): DeleteMessageAction => dispatch(deleteMessage(id)),
        setEditingMessage: (id?: string): SetEditingMessageAction => dispatch(setEditingMessage(id))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
