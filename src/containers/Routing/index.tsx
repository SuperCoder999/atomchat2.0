import React from "react";
import { Router, Switch, Route } from "react-router";
import { createBrowserHistory, BrowserHistory } from "history";
import Chat from "../Chat";
import NotFound from "../../scenes/NotFound";

class Routing extends React.Component {
    private static HISTORY: BrowserHistory = createBrowserHistory();

    public render(): JSX.Element {
        return (
            <Router history={Routing.HISTORY}>
                <Switch>
                    <Route path="/" exact component={Chat} />
                    <Route path="*" component={NotFound} />
                </Switch>
            </Router>
        );
    }
}

export default Routing;
