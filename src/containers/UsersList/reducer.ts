import { SET_USERS } from "./actionTypes";
import { SetUsersAction, UsersListState } from "../../types/redux";

const initalState: UsersListState = {
    users: []
}

type Action = SetUsersAction;

export default (state: UsersListState = initalState, action: Action): UsersListState => {
    switch (action.type) {
        case SET_USERS:
            {
                const actionConverted: SetUsersAction = (action as SetUsersAction);

                return {
                    users: actionConverted.users
                };
            }
        default:
            return state;
    }
};
