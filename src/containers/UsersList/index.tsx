import React from "react";
import { connect } from "react-redux";
import { State } from "../../types/redux";

interface UsersListProps {
    users: string[]
}

class UsersList extends React.Component<UsersListProps> {
    public shouldComponentUpdate(nextProps: UsersListProps): boolean {
        return nextProps.users.length !== this.props.users.length ||
            !nextProps.users.every((user: string, i: number): boolean => this.props.users[i] === user);
    }

    public render(): JSX.Element {
        return (
            <div style={{ textAlign: "center", marginTop: 30 }}>
                {this.props.users.map((user: string, index: number): JSX.Element => (
                    <React.Fragment key={index}>
                        <h5 className="inline">{user}</h5>
                        {index !== (this.props.users.length - 1) ? ", " : " "}
                    </React.Fragment>
                ))}
                are in chat
            </div>
        );
    }
}

function mapStateToProps(state: State): { users: string[] } {
    return {
        users: [...state.users.users]
    }
}

export default connect(mapStateToProps)(UsersList);
