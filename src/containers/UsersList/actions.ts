import { SET_USERS } from "./actionTypes";
import { SetUsersAction } from "../../types/redux";

export function setUsers(users: string[]): SetUsersAction {
    return {
        type: SET_USERS,
        users
    }
}
