import { Message, PartialMessage } from "../../types/message";
import { SET_MESSAGES, UPDATE_MESSAGE, ADD_MESSAGE, DELETE_MESSAGE, SET_EDITING_MESSAGE } from "./actionTypes";
import { SetMessagesAction, UpdateMessageAction, AddMessageAction, DeleteMessageAction, SetEditingMessageAction } from "../../types/redux";

export function setMessages(messages: Message[]): SetMessagesAction {
    return {
        type: SET_MESSAGES,
        messages
    };
}

export function updateMessage(id: string, newMessage: PartialMessage): UpdateMessageAction {
    return {
        type: UPDATE_MESSAGE,
        id,
        message: newMessage
    };
}

export function addMessage(message: Message): AddMessageAction {
    return {
        type: ADD_MESSAGE,
        message
    }
}

export function deleteMessage(id: string): DeleteMessageAction {
    return {
        type: DELETE_MESSAGE,
        id
    }
}

export function setEditingMessage(id?: string): SetEditingMessageAction {
    return {
        type: SET_EDITING_MESSAGE,
        id
    }
}
