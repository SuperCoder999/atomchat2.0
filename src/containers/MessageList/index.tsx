import React from "react";
import { Message } from "../../types/message";
import MessageUI from "../../components/Message";
import { Comment as Card, Divider, Button } from "semantic-ui-react";
import moment from "moment";
import { connect } from "react-redux";
import { State } from "../../types/redux";
import { MESSAGE_ME_USER_ID, UP_ARROW_KEY_CODE } from "../../config";

interface MessageListProps {
    messages: Message[];
    showingMessagesLimitStep: number;
    editMessage: (id: string) => void;
    deleteMessage: (id: string) => void;
    likeMessage: (id: string) => void;
    dislikeMessage: (id: string) => void;
}

interface MessageListState {
    showingMessagesLimit: number;
}

class MessageList extends React.Component<MessageListProps, MessageListState> {
    public constructor(props: MessageListProps) {
        super(props);

        this.state = {
            showingMessagesLimit: props.showingMessagesLimitStep
        }
    }

    public render(): JSX.Element {
        let previousLineDate: Date | null = null;
        const limitIndex: number = Math.min(this.props.messages.length, this.state.showingMessagesLimit);
        const messages: Message[] = this.props.messages
            .sort((message0: Message, message1: Message): number => message0.createdAt < message1.createdAt ? 1 : -1)
            .slice(0, limitIndex);
        
        const myMessageIndex: number = messages.findIndex((message: Message): boolean => message.userId === MESSAGE_ME_USER_ID);
        
        if (myMessageIndex >= 0) {
            const myMessageId: string = messages[myMessageIndex].id;

            document.onkeydown = (event: KeyboardEvent): void => {
                if (event.keyCode === UP_ARROW_KEY_CODE) {
                    this.props.editMessage(myMessageId);
                } 
            };
        }

        return (
            <Card.Group className="message-list">
                {messages.map((message: Message, index: number): JSX.Element => {
                    const canPutLine: boolean = previousLineDate ? message.createdAt.getDate() !== previousLineDate.getDate() : true;
                    if (canPutLine) previousLineDate = new Date(message.createdAt);

                    return (
                        <React.Fragment key={index}>
                            {canPutLine
                                ? <Divider horizontal>{moment(message.createdAt).format("LL")}</Divider>
                                : ""}
                            <MessageUI
                                message={{...message}}
                                editMessage={this.props.editMessage}
                                deleteMessage={this.props.deleteMessage}
                                like={this.props.likeMessage}
                                dislike={this.props.dislikeMessage}
                            />
                        </React.Fragment>
                    );
                })}

                {this.props.messages.length !== messages.length
                    ? <Button
                        primary
                        className="centered"
                        onClick={(): void => this.setState({
                            showingMessagesLimit: this.state.showingMessagesLimit + this.props.showingMessagesLimitStep
                        })}
                        style={{
                            marginTop: 20
                        }}
                    >
                        Show more
                    </Button>
                    : ""}
            </Card.Group>
        );
    }
}

function mapStateToProps(state: State): { messages: Message[] } {
    return {
        messages: state.messages.messages
    }
}

export default connect(mapStateToProps)(MessageList);
