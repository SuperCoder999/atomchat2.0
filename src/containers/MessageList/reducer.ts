import {
    SET_MESSAGES,
    UPDATE_MESSAGE,
    ADD_MESSAGE,
    DELETE_MESSAGE,
    SET_EDITING_MESSAGE
} from "./actionTypes";

import {
    SetMessagesAction,
    MessageListState,
    UpdateMessageAction,
    AddMessageAction,
    DeleteMessageAction,
    SetEditingMessageAction
} from "../../types/redux";

import { Message } from "../../types/message";

const initalState: MessageListState = {
    messages: []
}

type Action = SetMessagesAction | UpdateMessageAction | AddMessageAction | DeleteMessageAction | SetEditingMessageAction;

export default (state: MessageListState = initalState, action: Action): MessageListState => {
    switch (action.type) {
        case SET_MESSAGES:
            {
                return {
                    ...state,
                    messages: (action as SetMessagesAction).messages
                };
            }
        case UPDATE_MESSAGE:
            {
                const actionConverted: UpdateMessageAction = (action as UpdateMessageAction);
                const index: number = state.messages.findIndex((value: Message): boolean => actionConverted.id === value.id);

                return {
                    ...state,
                    messages: [
                        ...state.messages.slice(0, index),
                        {
                            ...state.messages[index],
                            ...actionConverted.message
                        },
                        ...state.messages.slice(index + 1, state.messages.length)
                    ]
                }
            }
        case ADD_MESSAGE:
            {
                const actionConverted: AddMessageAction = (action as AddMessageAction);
                
                return {
                    ...state,
                    messages: [
                        actionConverted.message,
                        ...state.messages
                    ]
                }
            }
        case DELETE_MESSAGE:
            {
                const actionConverted: DeleteMessageAction = (action as DeleteMessageAction);
                const messages: Message[] = [...state.messages];
                const index: number = messages.findIndex((message: Message): boolean => message.id === actionConverted.id);
                messages.splice(index, 1);

                return {
                    ...state,
                    messages
                }
            }
        case SET_EDITING_MESSAGE:
            {
                const actionConverted: SetEditingMessageAction = (action as SetEditingMessageAction);

                return {
                    ...state,
                    editingID: actionConverted.id
                }
            }
        default:
            return state;
    }
};
