import React from "react";
import { act } from "react-dom/test-utils";
import { render, unmountComponentAtNode } from "react-dom";
import NotFound from "./";

let container: HTMLDivElement | null = null;

beforeEach((): void => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

it("should show not found page", (): void => {
    act((): void => {
        render(<NotFound />, container);
    });

    expect(container!.innerHTML).toContain("Page not found");
})

afterEach((): void => {
    unmountComponentAtNode(container!);
    container!.remove();
    container = null;
});
