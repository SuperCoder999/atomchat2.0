import React from "react";
import { Grid, Button, Header } from "semantic-ui-react";

class NotFound extends React.Component {
    public render(): JSX.Element {
        return (
            <Grid textAlign="center" verticalAlign="middle" className="fill back-black">
                <Grid.Column style={{ maxWidth: 400 }}>
                    <Header color="red" as="h1">Oops! Page not found!</Header>
                    <Button positive fluid onClick={(): void => window.history.back()}>Back!</Button>
                </Grid.Column>
            </Grid>
        );
    }
}

export default NotFound;
