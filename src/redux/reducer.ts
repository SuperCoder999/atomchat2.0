import { combineReducers } from "redux";
import messages from "../containers/MessageList/reducer";
import users from "../containers/UsersList/reducer";

const reducer = combineReducers({ messages, users });

export default reducer;
