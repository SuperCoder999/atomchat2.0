import { genUUID } from "./helpers/uuid";

export const GET_MESSAGES_URL = "https://edikdolynskyi.github.io/react_sources/messages.json";
export const MESSAGE_ME_USER = "Me";
export const MESSAGE_ME_USER_ID = genUUID();
const IS_MOBILE_REGEX: RegExp = /Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/;
export const IS_MOBILE: boolean = IS_MOBILE_REGEX.test(navigator.userAgent);
export const UP_ARROW_KEY_CODE = 38;
