import { GET_MESSAGES_URL } from "../config";
import { Message, NotCompiledMessage, compileMessage } from "../types/message";

export async function getMessages(): Promise<Message[]> {
    const response: Response = await fetch(GET_MESSAGES_URL);
    const json: NotCompiledMessage[] = await response.json();
    const messages = json.map(compileMessage);

    return messages;
}
