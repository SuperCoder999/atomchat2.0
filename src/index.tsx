import React from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";
import Home from "./scenes/Home";

import "./styles/reset.scss";
import "semantic-ui-css/semantic.min.css";
import "./styles/common.scss";

console.warn("This app is a prototype");

ReactDOM.render(
    <Home />,
    document.getElementById("root")
);

serviceWorker.register();
