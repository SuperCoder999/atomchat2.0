import React from "react";
import { Dimmer, Loader } from "semantic-ui-react";

class Spinner extends React.Component {
    public shouldComponentUpdate(): boolean {
        return false;
    }

    public render(): JSX.Element {
        return (
            <Dimmer active>
                <Loader inverted size="massive" />
            </Dimmer>
        );
    }
}

export default Spinner;
