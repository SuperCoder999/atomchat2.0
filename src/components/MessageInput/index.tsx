import React from "react";
import { Form, Button, TextAreaProps } from "semantic-ui-react";

interface MessageInputProps {
    createMessage: (text: string) => void
}

interface MessageInputState {
    messageText: string;
}

class MessageInput extends React.Component<MessageInputProps, MessageInputState> {
    constructor(props: MessageInputProps) {
        super(props);

        this.state = {
            messageText: ""
        }
    }

    public shouldComponentUpdate(nextProps: MessageInputProps, nextState: MessageInputState): boolean {
        return nextState.messageText !== this.state.messageText;
    }

    private setMessage(text: string | null): void {
        this.setState({
            messageText: text || ""
        });
    }

    public render(): JSX.Element {
        return (
            <Form
                onSubmit={(): void => {
                    if (this.state.messageText) {
                        this.props.createMessage(this.state.messageText);
                        this.setMessage("");
                        (document.querySelector("#messageArea") as HTMLTextAreaElement).value = "";
                    }
                }}
                className="centered max-300"
            >
                <Form.TextArea
                    placeholder="Message"
                    defaultValue=""
                    id="messageArea"
                    autoFocus
                    onChange={(event: React.FormEvent, data: TextAreaProps): void => this.setMessage((data.value as string | null))}
                />
                <Button primary fluid disabled={!Boolean(this.state.messageText)}>Add message</Button>
            </Form>
        );
    }
}

export default MessageInput;
