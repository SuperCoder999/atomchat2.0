import React from "react";
import { Statistic, Header as HeaderUI, Grid } from "semantic-ui-react";
import { IS_MOBILE } from "../../config";
import moment from "moment";
import { Message } from "../../types/message";
import { connect } from "react-redux";
import { State } from "../../types/redux";
import { setUsers } from "../../containers/UsersList/actions";
import { SetUsersAction } from "../../types/redux";
import { Dispatch } from "redux";

interface HeaderProps {
    messages: Message[];
    setUsers: (users: string[]) => void
}

class Header extends React.Component<HeaderProps> {
    private updateUsers() {
        const { messages } = this.props;
        this.props.setUsers(Array.from(new Set(messages.map((message: Message): string => message.user))));
    }

    public componentDidUpdate() {
       this.updateUsers();
    }

    public componentDidMount() {
        this.updateUsers();
    }

    public render(): JSX.Element {
        const { messages } = this.props;
        const messagesUsers: string[] = messages.map((message: Message): string => message.userId);
        const messagesUsersSet: Set<string> = new Set(messagesUsers);
        const participantsCount: number = messagesUsersSet.size;
        const messagesCount: number = messages.length;

        const messagesSortedByCreationDate: Message[] = messages.sort((message0: Message, message1: Message): number => {
            return message0.createdAt < message1.createdAt ? 1 : -1;
        });

        let latestMessageDate: Date | null = null;

        if (messages.length > 0) {
            const latestMessage: Message = messagesSortedByCreationDate[0];
            latestMessageDate = latestMessage.createdAt;
        }

        const statisticSize: "tiny" | "small" | "mini" | "large" | "huge" = IS_MOBILE ? "tiny" : "small";

        const peopleStatistic: JSX.Element = <Statistic size={statisticSize}>
            <Statistic.Value>{participantsCount}</Statistic.Value>
            <Statistic.Label>People</Statistic.Label>
        </Statistic>;

        if (IS_MOBILE) {
            return (
                <div className="fluid site-header">
                    <Grid columns="3" textAlign="center" verticalAlign="middle" className="fill">
                        <Grid.Column>
                           {peopleStatistic} 
                        </Grid.Column>
                        <Grid.Column>
                            <HeaderUI className="inline no-break" as="h3" style={{ color: "#0e9ad2" }}>Atom Chat</HeaderUI>
                        </Grid.Column>
                        <Grid.Column>
                            <Statistic size={statisticSize}>
                                <Statistic.Value>{messagesCount}</Statistic.Value>
                                <Statistic.Label>Messages</Statistic.Label>
                            </Statistic>
                        </Grid.Column>
                    </Grid>
                </div>
            );
        }

        return (
            <div className="fluid site-header">
                <HeaderUI as="h1" className="inline">Atom Chat</HeaderUI>
                <Statistic.Group size={statisticSize} className="inline">
                    {peopleStatistic}
                    <Statistic>
                        <Statistic.Value>{messagesCount}</Statistic.Value>
                        <Statistic.Label>
                            Messages - latest
                            {" "}
                            {latestMessageDate
                                ? moment(latestMessageDate).fromNow()
                                : ""}
                        </Statistic.Label>
                    </Statistic>
                </Statistic.Group>
            </div>
        );
    }
}

function mapStateToProps(state: State): { messages: Message[] } {
    return {
        messages: state.messages.messages
    }
}

type A = SetUsersAction;

function mapDispatchToProps(dispatch: Dispatch<A>) {
    return {
        setUsers: (users: string[]): SetUsersAction => dispatch(setUsers(users))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
